import numpy as np
import streamlit as st
import tensorflow
from tensorflow.keras.models import model_from_json
from PIL import Image, ImageOps
from webapp import import_and_predict

st.set_option('deprecation.showfileUploaderEncoding', False)

st.title("""
         COVID DETECTION TOOL
         """
         )

html_temp = """
    <div style="background-color:tomato;padding:10px">
    <h2 style="color:white;text-align:center;">Lung CT Scan - Coronavirus Prediction</h2>
    </div>
    """
st.markdown(html_temp,unsafe_allow_html=True)
st.write("")
st.write("This is a Lung CT Scan image classification application. This is being designed to be used by doctors and not general public")

file = st.file_uploader("Please upload an image file", type=["jpg", "png"])
#
if file is None:
    st.text("You haven't uploaded an image file")
else:
    image = Image.open(file)
    st.image(image, use_column_width=True)
    prediction = import_and_predict(image)
    
    if np.argmax(prediction) == 0:
        st.write("Coronavirus Positive!")
    elif np.argmax(prediction) == 1:
        st.write("Coronavirus Negative!")
    
    st.text("Probability (0: Corona Positive, 1: Corona Negative)")
    st.write(prediction)

